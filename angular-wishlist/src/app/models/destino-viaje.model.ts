import { v4 as uuid } from 'uuid';

export class DestinoViaje {
    private selected: boolean;
    public services: string[];
    id = uuid();

    constructor(public nombre:string, public u:string, public votes: number = 0) { 
        this.services = ['Desayuno', 'Botella de vino'];
    }

    setSelected(s: boolean) {
        this.selected = s;
    }

    isSelected() {
        return this.selected;
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        this.votes--;
    }


}