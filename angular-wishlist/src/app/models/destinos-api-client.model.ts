import { DestinoViaje } from './destino-viaje.model';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';

@Injectable()
export class DestinoApiClient {

    constructor(private store: Store<AppState>){
    }

    add(d: DestinoViaje){
        this.store.dispatch(new NuevoDestinoAction(d));
    }

    elegir(d: DestinoViaje){
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }
}